import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScienceComponent, SportsComponent, WeatherComponent } from './components';
import { Page1Component, Page2Component, Page3Component } from './pages';

const routes: Routes = [
  {
    path: 'science',
    component: ScienceComponent,
    children: [
      { path: 'page1', component: Page1Component },
      { path: 'page2', component: Page2Component },
      { path: 'page3', component: Page3Component },
      { path: '', redirectTo: '/news/science/page1', pathMatch: 'full' },
      { path: '**', redirectTo: '/news/science/page1', pathMatch: 'full' },
    ],
  },
  {
    path: 'sports',
    component: SportsComponent,
    children: [
      { path: 'page1', component: Page1Component },
      { path: 'page2', component: Page2Component },
      { path: 'page3', component: Page3Component },
      { path: '', redirectTo: '/news/sports/page1', pathMatch: 'full' },
      { path: '**', redirectTo: '/news/sports/page1', pathMatch: 'full' },
    ],
  },
  {
    path: 'weather',
    component: WeatherComponent,
    children: [
      { path: '1', component: Page1Component },
      { path: '2', component: Page2Component },
      { path: '3', component: Page3Component },
      { path: '', redirectTo: '/news/weather/1', pathMatch: 'full' },
      { path: '**', redirectTo: '/news/weather/1', pathMatch: 'full' },
    ],
  },
  { path: '', redirectTo: '/news', pathMatch: 'full' },
  { path: '**', redirectTo: '/news', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsRoutingModule {}
