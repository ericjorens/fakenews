import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ScienceComponent,
  SportsComponent,
  WeatherComponent,
} from './components';
import { Page1Component, Page2Component, Page3Component } from './pages';
import { NewsRoutingModule } from './news-routing.module';

@NgModule({
  declarations: [
    Page1Component,
    Page2Component,
    Page3Component,
    ScienceComponent,
    SportsComponent,
    WeatherComponent,
  ],
  imports: [CommonModule, NewsRoutingModule],
  exports: [Page1Component, Page2Component, Page3Component],
})
export class NewsModule {}
