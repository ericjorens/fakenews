import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReaderComponent } from './components/reader/reader.component';
import { Page1Component } from './modules/news/pages';

const routes: Routes = [
  { path: 'reader', component: ReaderComponent },
  { path: 'reader/:id', component: ReaderComponent },
  { path: 'page1', component: Page1Component },
  {
    path: 'news',
    loadChildren: () =>
      import('./modules/news/news.module').then((m) => m.NewsModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
