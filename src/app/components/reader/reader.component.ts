import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css'],
})
export class ReaderComponent implements OnInit {
  name: string;
  val: any;
  id: string | null;

  constructor(private route: ActivatedRoute) {
    this.name = '';
    this.val = 0;
    this.id = '';
  }

  /**
   * Subscribe here to the route query and path params. See app-routing.module.ts for how this route is set up
   */
  ngOnInit(): void {
    //for example, http://localhost:4200/reader?name=test&val=2
    this.route.queryParams.subscribe((params) => {
      this.name = params['name'];
      this.val = params['val'];
    });

    //for example, http://localhost:4200/reader/1
    this.route.paramMap.subscribe((params: ParamMap) => {
      if (!!params) {
        if (!!params.get('id')) {
          this.id = params.get('id');
        }
      }
    });
  }
}
